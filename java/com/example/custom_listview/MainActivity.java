package com.example.custom_listview;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {
ListView listView;
int Image[]={R.drawable.user,R.drawable.user,R.drawable.user,R.drawable.user,R.drawable.user,R.drawable.user,R.drawable.user};
String name[]={"Pramod Verma","Manglesh Verma","Aniket Verma","Nikita Verma","Parinati Thakur","Manish Verma","Ananad Verma","Rinki Verma"};
String [] mob={"8359968242","7447019182","9752089615","9174789611","8319189862","8120044416","9754478410","8359968242"};
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        listView =findViewById(R.id.listview);
        CustomAdaptor customAdaptor =new CustomAdaptor();
        listView.setAdapter(customAdaptor);
    }
    class CustomAdaptor extends BaseAdapter
    {

        @Override
        public int getCount() {
            return Image.length;
        }

        @Override
        public Object getItem(int position) {
            return null;
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
           View view =getLayoutInflater().inflate(R.layout.cutom_listview,null);
            ImageView mimage = view.findViewById(R.id.img);
            TextView mtext =view.findViewById(R.id.text);
            TextView textView=view.findViewById(R.id.mob);
            mimage.setImageResource(Image[position]);
            mtext.setText(name[position]);
            textView.setText(mob[position]);
            return view;
        }
    }
}
